using System.Collections.Generic;

namespace Extension
{
	public static class Extension
	{
	    public static void Swap<Task>(this List<Task> list, int i, int j)
	    {
	         Task temp = list[i];
	         list[i] = list[j];
	         list[j] = temp;
	    }
	}	
}
