﻿using System;
using System.Collections.Generic;

namespace DSM_ACO
{
    public class Task : IComparable<Task>
    {
        public string Description { get; set; }
        public TimeCurve ConsumptionCurve { get; set; }
        public Task Previous { get; set; }
        public int? ReleaseTime { get; set; } // minimum 1
        public int? Deadline { get; set; } // maximum 1440. Deadline is inclusive, meaning the task still runs on the deadline minute
        public int? Duration { get; set; }
        public int? Interval { get; set; }

        public int CompareTo(Task other)
        {
          return (this.Interval ?? 0).CompareTo(other.Interval);
        }
        
        public Task(string descr, TimeCurve consumption, int? release = null, int? deadline = null, int? duration = null, Task prev = null)
        {
            Description = descr;
            ConsumptionCurve = consumption;  //Must start at minute 1 and last as long as the duration, this needs to be checked
            Previous = prev;
            ReleaseTime = release;
            Deadline = deadline;
            Duration = duration;
            Interval = Deadline - ReleaseTime - Duration;
        }

        public static Task MakeMicrowaveTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(800, 1, 10)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 10);
        }

        public static Task[] MakeDishwasherTask(string descr, int release, int deadline)
        {
            var tasks = new Task[3];

            var ci1 = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(1000, 1, 30)
            };

            var ci2 = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(500, 1, 30)
            };

            var ci3 = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(900, 1, 30)
            };

            var c1 = new TimeCurve(ci1);
            var c2 = new TimeCurve(ci2);
            var c3 = new TimeCurve(ci3);
            tasks[0] = new Task(descr + " - Parte 0", c1, release, deadline-60, 30);
            tasks[1] = new Task(descr + " - Parte 1", c2, release+30, deadline-30, 30, tasks[0]);
            tasks[2] = new Task(descr + " - Parte 2", c3, release+60, deadline, 30, tasks[1]);

            return tasks;
        }

        public static Task MakeWashingMachineTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(500, 1, 60),
                new TimeCurve.CurveInterval(300, 61, 120)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 120);
        }

        public static Task MakeOvenTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(1000, 1, 45)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 45);
        }

        public static Task MakeDryingMachineTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(900, 1, 30),
                new TimeCurve.CurveInterval(1200, 31, 120)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 120);
        }

        public static Task[] MakeEsdTask(string descr, int release, int deadline)
        {
            var tasks = new Task[10];

            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(1000, 1, 30)
            };

            var c = new TimeCurve(ci);
            tasks[0] = new Task(descr + " - Parte 0", c, release, deadline-270, 30);
            tasks[1] = new Task(descr + " - Parte 1", c, release+30, deadline-240, 30, tasks[0]);
            tasks[2] = new Task(descr + " - Parte 2", c, release+60, deadline-210, 30, tasks[1]);
            tasks[3] = new Task(descr + " - Parte 3", c, release+90, deadline-180, 30, tasks[2]);
            tasks[4] = new Task(descr + " - Parte 4", c, release+120, deadline-150, 30, tasks[3]);
            tasks[5] = new Task(descr + " - Parte 5", c, release+150, deadline-120, 30, tasks[4]);
            tasks[6] = new Task(descr + " - Parte 6", c, release+180, deadline-90, 30, tasks[5]);
            tasks[7] = new Task(descr + " - Parte 7", c, release+210, deadline-60, 30, tasks[6]);
            tasks[8] = new Task(descr + " - Parte 8", c, release+240, deadline-30, 30, tasks[7]);
            tasks[9] = new Task(descr + " - Parte 9", c, release+270, deadline, 30, tasks[8]);

            return tasks;
        }

        public static Task MakeAirConditionerTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(1000, 1, 10)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 10);
        }

        public static Task MakeHeatPumpTask(string descr, int release, int deadline)
        {
            var ci = new List<TimeCurve.CurveInterval>
            {
                new TimeCurve.CurveInterval(2000, 1, 10)
            };
            var c = new TimeCurve(ci);
            return new Task(descr, c, release, deadline, 10);
        }
    }
}
