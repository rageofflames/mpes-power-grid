﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using Extension;

namespace DSM_ACO
{
    public class SchedulerAco
    {
        private const int Epsilon1 = 1000;
        private const float Epsilon2 = 0.001f;
        private const int Epsilon3 = 1;
        private const int ResetThreshold = 500; //5000
        private const int MaxIter = 100000; //200
        private const int ElitistAntThreshold = 1; //1
        private const int MaxTime = 15;
        private Random r = new Random();

        public struct StartEndTime
        {
            public int Start;
            public int End;

            public StartEndTime(int start, int end)
            {
                Start = start;
                End = end;
            }
        }

        public class Ant
        {
            public List<KeyValuePair<Task,StartEndTime>> StartingTimes { get; set; }
            private float _cost = -1;
            
            private bool _powerCalculated;
            public float[] PowerRemaining { get; set; }

            public string ToString(float[,] costsM)
            {
                var str = "Schedule is:\n";

                for (var i = 0; i < StartingTimes.Count; i++)
                {
                    var t = StartingTimes[i];
                    int hour;
                    if (t.Value.Start > 300)
                        hour = (t.Value.Start - 301)/60;
                    else hour = (t.Value.Start-1)/60 + 19;

                    var minute = (t.Value.Start-1)%60;
                    str += "Task " + t.Key.Description + " starts at " + hour.ToString("D2") + ":" + minute.ToString("D2") + " with cost " + costsM[i,t.Value.Start-1]/60000 + "\n";
                }
                return str;
            }

            public void CalculateRemainingPower(TimeCurve power)
            {
                for (var i = 1; i <= PowerRemaining.Length; i++)
                {
                    var availablePower = power.GetValueAtTime(i);
                    for (var j = 0; j < StartingTimes.Count; j++) 
                    {
                        var t = StartingTimes[j];
                        if (t.Value.Start <= i && t.Value.End > i)
                        {
                            availablePower -= t.Key.ConsumptionCurve.GetValueAtTime(1 + i - t.Value.Start);
                        }
                    }

                    PowerRemaining[i - 1] = availablePower;
                }

                _powerCalculated = true;
            }

            public void AddStartingTime(KeyValuePair<Task, StartEndTime> st, TimeCurve power)
            {
                StartingTimes.Add(st);
                for (var i = st.Value.Start; i < st.Value.End; i++)
                {
                    var tPower = st.Key.ConsumptionCurve.GetValueAtTime(1 + i - st.Value.Start);

                    var availablePower = PowerRemaining[i-1];
                    availablePower -= tPower;

                    PowerRemaining[i - 1] = availablePower;
                }
            }

            public List<string> TaskHours()
            {
                var hrs = new List<string>();
                for (var i = 0; i < StartingTimes.Count; i++)
                {
                    var t = StartingTimes[i];
                    int hour;
                    if (t.Value.Start > 300)
                        hour = (t.Value.Start - 301) / 60;
                    else hour = (t.Value.Start - 1) / 60 + 19;

                    var minute = (t.Value.Start - 1) % 60;
                    var hr = "";
                    hr += hour.ToString("D2") + ":" + minute.ToString("D2");
                    hrs.Add(hr);
                }
                return hrs;
            }

            /*
             * Calculates the cost of this ant's solution taking into account the tasks' starting times,
             * consumption curves, pricing information, previous tasks and available power.
             */
            public float Cost(TimeCurve pricing, TimeCurve power)
            {
                throw new NotImplementedException();
            }

            /*
             * Same as above except instead of the pricing information it uses a pre-calculated cost matrix to speed
             * up the calculation.
             */
            public float Cost(float[,] costsM, TimeCurve power, bool useCache = true)
            {
                if (Math.Abs(_cost - (-1)) < 1)
                {
                    if (!_powerCalculated || !useCache)
                        CalculateRemainingPower(power);

                    for (var i = 0; i < PowerRemaining.Length; i++)
                    {
                        var availablePower = PowerRemaining[i];

                        if (availablePower < 0)
                            return float.MaxValue;
                    }

                    if (StartingTimes.Any(t => t.Key.Previous != null && t.Value.Start < StartingTimes.Find(p => p.Key == t.Key.Previous).Value.End))
                        return float.MaxValue;

                        var cost = StartingTimes.Select((t, i) => costsM[i, t.Value.Start-1]).Sum();

                    if (useCache)
                        _cost = cost;
                    else return cost;
                }

                return _cost;
            }

            //This is just for the server, don't use in calculations
            public float Cost()
            {
                return _cost;
            }

            public Ant()
            {
                PowerRemaining = new float[1440];
                StartingTimes = new List<KeyValuePair<Task, StartEndTime>>();
            }
        }

        public float bestSolution {get; set; }
        public List<Task> Tasks { get; set; }
        public TimeCurve Pricing { get; set; }
        public TimeCurve Power { get; set; }
        public int AntNum { get; set; }
        public float Alpha { get; set; }
        public float Beta { get; set; }
        public float EvaporationFactor { get; set; }
        public float PheromoneDepositingFactor { get; set; }
        public float A { get; set; }
        public float B { get; set; }
        public bool Elitist { get; set; }
        public int MaximumCost { get; set; }
        public int MinimumCost { get; set; }
        public float[] MaximumTaskCosts { get; set; }
        public float[] MinimumTaskCosts { get; set; }
        public bool ResetPheromonesWhenStuck { get; set; }
        public bool ReinforceBestTrail { get; set; }

        public SchedulerAco(List<Task> tasks, TimeCurve pricing, TimeCurve power, int numAnts, 
                                float alpha, float beta, float rho, float q, bool elitist, bool reset, bool reinforceBest)
        {
            Contract.Requires(power != null);
            Contract.Requires(pricing != null);
            Contract.Requires(pricing.GetStartingTime() == 1);
            Contract.Requires(pricing.GetEndingTime() == 1440);
            Contract.Requires(power.GetStartingTime() == 1);
            Contract.Requires(power.GetEndingTime() == 1440);
            Contract.Requires(rho >= 0);
            Contract.Requires(rho <= 1);

            SortListWithPreemptive(tasks);

            Tasks = tasks;
            Pricing = pricing;
            Power = power;
            AntNum = numAnts;
            Alpha = alpha;
            Beta = beta;
            EvaporationFactor = rho;
            PheromoneDepositingFactor = q;
            Elitist = elitist;
            ResetPheromonesWhenStuck = reset;
            ReinforceBestTrail = reinforceBest;
        }

        public void SortListWithPreemptive(List<Task> tasks)
        {

            tasks.Sort();

            for (int i = 0; i < tasks.Count; i++)
            {
                Task task = tasks[i];

                if (task.Previous != null)
                {
                    int index = tasks.IndexOf(task);
                    int indexOfPrevious = tasks.IndexOf(task.Previous);

                    if (indexOfPrevious > index)
                    {
                        tasks.Swap(index, indexOfPrevious);
                        i--;
                    }
                }
            }
        }

        public Ant Solve()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            var costsM = MakeCostsMatrix();
            var pheromonesM = InitPheromonesMatrix();
            CalculateMinMaxPossibleCosts(costsM);
            CalculateLogScale();

            var ants = InitAnts(costsM, pheromonesM);
            float bestCost;
            var bestS = BestSolution(ants, out bestCost, costsM);

            var iter = 0;
            var sameBest = 0;
            Ant prevBestS = null;
            while (!StoppingCriterion(iter, watch.Elapsed.Seconds))
            {
                UpdateAnts(ants, pheromonesM, costsM);
                UpdatePheromones(pheromonesM, ants, costsM, bestS);

                float currBestCost;
                var currBestS = BestSolution(ants, out currBestCost, costsM);
                if (currBestCost < bestCost)
                {
                    bestCost = currBestCost;
                    bestS = currBestS;
                }

                if (ResetPheromonesWhenStuck)
                {
                    if (prevBestS == null)
                        prevBestS = bestS;
                    else
                    {
                        if (prevBestS == bestS)
                        {
                            sameBest++;
                            if (sameBest >= ResetThreshold)
                            {
                                Console.WriteLine("Solution stuck. Pheromones Reset!");
                                pheromonesM = InitPheromonesMatrix();
                                sameBest = 0;
                            }
                        }
                        else
                            sameBest = 0;

                        prevBestS = bestS;
                    }
                }

                /*Console.WriteLine("Tested a solution with cost: " + currBestCost/60000);
                Console.WriteLine("Currently on iteration: " + iter);
                Console.WriteLine("Current cost is: " + bestCost/60000);
                Console.WriteLine("-------------");*/
     
                iter++;
            }

            watch.Stop();

            Console.WriteLine(iter);
            //Console.WriteLine(bestS.ToString(costsM));
            bestSolution = bestS.Cost(costsM, Power)/60000;

            return bestS;
        }

        /*
         * Updates the pheromone matrix according to the current ants and the equation
         * Fij = Fij + sumOf( DepositingFactor / cost of solution) for every ant that starts task i at time j
         */
        private void UpdatePheromones(float[,] pheromonesM, Ant[] ants, float[,] costsM, Ant best)
        {
            Parallel.For(0, pheromonesM.GetLength(0), i =>
            {
                for (var j = 0; j < pheromonesM.GetLength(1); j++)
                {
                    pheromonesM[i, j] = pheromonesM[i, j]*(1 - EvaporationFactor);
                }
            });
            

            var dif = (float)(MaximumCost - MinimumCost);
            if (Elitist)
                ants = ants.OrderBy(a => a.Cost(costsM, Power)).ToArray();

            Parallel.ForEach(ants, ant =>
            {
                for (var i = 0; i < ant.StartingTimes.Count; i++)
                {
                    if (!Elitist || i < ElitistAntThreshold)
                        //in elitist mode only 2 current best ants deposit pheromones in addition to overall best ant
                    {
                        var cost = ant.Cost(costsM, Power);
                        if (Math.Abs(cost - float.MaxValue) < Epsilon1)
                            break;
                        var c = PheromoneDepositingFactor - ((cost - MinimumCost)*PheromoneDepositingFactor)/dif;
                        pheromonesM[i, ant.StartingTimes[i].Value.Start - 1] += (float)(A*Math.Exp(B*c));
                    }
                }
            });

            if (ReinforceBestTrail)
            {
                for (var i = 0; i < best.StartingTimes.Count; i++)
                {
                    var cost = best.Cost(costsM, Power);
                    if (Math.Abs(cost - float.MaxValue) < Epsilon1)
                        break;
                    var c = PheromoneDepositingFactor - ((cost - MinimumCost)*PheromoneDepositingFactor)/dif;
                    pheromonesM[i, best.StartingTimes[i].Value.Start - 1] += (float)(A * Math.Exp(B * c));
                }
            }
        }

        /*
         * Updates the ants, generating new solutions via a roulette wheel selection method for the
         * starting times of each task. For each task the probability of choosing a given starting time
         * with a partial solution S is P(T ij| S) = (Pij^alpha * Hij^beta) / SumOf(Pij^alpha * Hij^beta) for all times j
         */
        private void UpdateAnts(Ant[] ants, float[,] pheromonesM, float[,] costsM)
        {
            Parallel.For(0, ants.Length, i =>
            {
                var newAnt = new Ant();
                newAnt.CalculateRemainingPower(Power);

                for (var j = 0; j < Tasks.Count; j++)
                {

                    var t = Tasks[j];
                    if (t.Deadline == null || t.ReleaseTime == null || t.Duration == null)
                        throw new Exception("Task release time/Deadline/Duration not initialized.");


                    var numberOfStartingTimes = (t.Deadline ?? 0) - (t.ReleaseTime ?? 0) - (t.Duration ?? 0) + 2;
                        //two needs to be added because of how we count the starting/ending times and duration

                    float sumOfProbs = 0;
                    var probabilities = new float[numberOfStartingTimes];

                    for (var prob = 0; prob < probabilities.Length; prob++)
                    {
                        var h = HeuristicValue(newAnt, j, costsM, prob + (t.ReleaseTime ?? 0));
                        if (Math.Abs(h) < Epsilon2)
                            probabilities[prob] = 0;
                        else
                        {
                            var ph = pheromonesM[j, prob + (t.ReleaseTime ?? 0)];
                            var pb = (float)Math.Pow(h, Beta);
                            var pa = (float)Math.Pow(ph, Alpha);
                            probabilities[prob] = pa + pb;
                            sumOfProbs += probabilities[prob];
                        }
                    }

                    var randomNb = (float)r.NextDouble();

                    float sum = 0;
                    for (var prob = 0; prob < probabilities.Length; prob++)
                    {
                        probabilities[prob] = sum + probabilities[prob]/sumOfProbs;
                        sum = probabilities[prob];

                        if (prob == probabilities.Length - 1)
                            probabilities[prob] = 1;

                        if (randomNb < probabilities[prob])
                        {
                            var start = prob + (t.ReleaseTime ?? 0);
                            var end = start + (t.Duration ?? 0);
                            newAnt.AddStartingTime(new KeyValuePair<Task, StartEndTime>(t, new StartEndTime(start, end)), Power);
                            break;
                        }
                    }
                }

                ants[i] = newAnt;
            });
        }

        /*
         * Calculates a heuristic value for how good a starting time is for a certain task given a certain partial solution
         */
        private float HeuristicValue(Ant ant, int tNum, float[,] costsM, int startingTime) //startingTime starts at 1
        {
            var dif = MaximumTaskCosts[tNum] - MinimumTaskCosts[tNum];
            var task = Tasks[tNum];
            var endingTime = startingTime + task.Duration;

            if (Math.Abs(costsM[tNum, startingTime - 1] - float.MaxValue) < Epsilon1)
                return 0;

            foreach (var t in ant.StartingTimes)
                if (t.Key == task.Previous && t.Value.End > startingTime)
                    return 0;

            for (var i = startingTime; i < endingTime; i++)
            {
                var availablePowerA = ant.PowerRemaining[i - 1] - task.ConsumptionCurve.GetValueAtTime(1 + i - startingTime);

                if (availablePowerA < 0)
                    return 0;
            }

            if (Math.Abs(MinimumTaskCosts[tNum] - MaximumTaskCosts[tNum]) < Epsilon3)
                return 1;

            var cost = costsM[tNum, startingTime - 1];
            var c = PheromoneDepositingFactor - ((cost - MinimumTaskCosts[tNum]) * PheromoneDepositingFactor) / dif;
            if (Math.Abs(c) > Epsilon2)
                return (float)(A*Math.Exp(B*c));
                //return c;
            return 1;
        }

        /*
         * Stop the calculation based on number of iterations or current best solution cost
         */
        private static bool StoppingCriterion(int iter, int seconds)
        {
            if (iter > MaxIter || seconds >= MaxTime) //simple starting criterion for stopping
                return true;

            return false;
        }

        /*
         * Initiates a pheromone matrix with numTasks * 1440 cells and initial value of 0 for every cell.
         * Tasks in the matrix should have the same order as the task list in the scheduler.
         */
        private float[,] InitPheromonesMatrix()
        {
            if (Tasks != null)
            {
                var matrix = new float[Tasks.Count,1440];

                return matrix;
            }

            throw new Exception("No tasks in scheduler.");
        }

        /*
         * Calculates the cost of solution for the ant by calling its cost function and passing it the power
         * curve and pricing information or cost matrix
         */
        public float Cost(Ant ant, float[,] costsM = null)
        {
            throw new NotImplementedException();
        }

        /*
         * Calculates the best solution and its cost by calling the cost function on all ants and passing it
         * the power curve and pricing information or cost matrix
         */
        private Ant BestSolution(Ant[] ants, out float bestCost, float[,] costsM = null)
        {
            Ant best = null;
            var bestC = float.MaxValue;
            foreach (var a in ants)
            {
                if (best == null)
                {
                    best = a;
                    bestC = a.Cost(costsM, Power);
                }
                else
                {
                    var possibleBestC = a.Cost(costsM, Power);
                    if (possibleBestC < bestC)
                    {
                        bestC = possibleBestC;
                        best = a;
                    }
                }
            }

            bestCost = bestC;
            return best;
        }

        /*
         * Initiates the ants either randomly or through some heuristic
         */
        private Ant[] InitAnts(float [,] costsM, float[,] pheromonesM)
        {
            var ants = new Ant[AntNum];
            
            UpdateAnts(ants, pheromonesM, costsM);

            return ants;

        }

        /*
         * Pre-calculates the costs of choosing any starting time for any task without taking into account
         * power constraints. Stores the result in a matrix with numTasks * 1440 cells. Tasks in the matrix
         * should have the same order as the task list in the scheduler.
         */
        private float[,] MakeCostsMatrix()
        {
            if (Tasks != null)
            {
                var matrix = new float[Tasks.Count,1440];

                for(var i = 0; i < matrix.GetLength(0); i++)
                {
                    for (var j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (j+1 < Tasks[i].ReleaseTime || j+1 > (Tasks[i].Deadline - (Tasks[i].Duration - 1))) //j+1 because we start counting at 1 for minutes
                        {
                            matrix[i,j] = float.MaxValue;
                        }
                        else
                        {
                            float cost = 0;
                            for (var taskT = 0; taskT < Tasks[i].Duration; taskT++)
                            {
                                if (Tasks[i].ReleaseTime == null)
                                    throw new Exception("Release time not assigned.");

                                var minuteOfDay = taskT + j + 1;
                                cost += Pricing.GetValueAtTime(minuteOfDay) * Tasks[i].ConsumptionCurve.GetValueAtTime(taskT+1); //again, because we start counting from 1
                            }

                            matrix[i,j] = cost;
                        }
                    }
                }

                return matrix;
            }

            throw new Exception("No tasks in scheduler.");
        }

        private void CalculateMinMaxPossibleCosts(float[,] costsM)
        {
            MinimumTaskCosts = new float[Tasks.Count];
            MaximumTaskCosts = new float[Tasks.Count];
            var costMin = 0.0;
            var costMax = 0.0;
            for (var i = 0; i < costsM.GetLength(0); i++)
            {
                var max = float.MinValue;
                var min = float.MaxValue;
                for (var j = (Tasks[i].ReleaseTime??0)-1; j < 1+(Tasks[i].Deadline??0)-(Tasks[i].Duration??0); j++)
                {
                    if (costsM[i, j] > max)
                        max = costsM[i, j];
                    if (costsM[i, j] < min)
                        min = costsM[i, j];
                }

                costMax += max;
                costMin += min;
                MinimumTaskCosts[i] = min;
                MaximumTaskCosts[i] = max;
            }

            MinimumCost = (int) costMin;
            MaximumCost = (int) costMax;
        }

        private void CalculateLogScale()
        {
            B = (float) (Math.Log(this.PheromoneDepositingFactor/0.1)/(this.PheromoneDepositingFactor - 0.1));
            A = (float)(this.PheromoneDepositingFactor/Math.Exp(B*this.PheromoneDepositingFactor));
        }
    }
}