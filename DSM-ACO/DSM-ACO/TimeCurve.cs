﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace DSM_ACO
{
    public class TimeCurve
    {
        public class CurveInterval {
            public float Value { get; set; }
            public int StartingPoint { get; set; }
            public int EndingPoint { get; set; }

            public CurveInterval(float value, int starting,int ending )
            {
                Value = value;
                StartingPoint = starting;
                EndingPoint = ending;
            }
        }

        private CurveInterval[] _curve;
        public CurveInterval[] Curve
        {
            get { return _curve; }
            set
            {
                _curve = value;
                //Contract.Ensures(CheckCurve(Curve)); //Put this back later
            }
        }

        [Pure]
        public static bool CheckCurve(IEnumerable<CurveInterval> curveToCheck)
        {
            var curveIntervals = curveToCheck as CurveInterval[] ?? curveToCheck.ToArray();
            if (!curveIntervals.Any())
                return false;

            CurveInterval previous = null;

            foreach (var c in curveIntervals)
            {
                if (c.StartingPoint > c.EndingPoint)
                    return false;

                if (previous == null)
                {
                    if (c.StartingPoint != 1)
                        return false;
                }
                else if (c.StartingPoint <= previous.EndingPoint || c.StartingPoint > previous.EndingPoint + 1)
                    return false;

                previous = c;
            }

            return true;
        }

        public TimeCurve(List<CurveInterval> curve = null)
        {
            Contract.Requires(CheckCurve(curve));

            Curve = curve != null ? curve.ToArray() : null;
        }

        [Pure]
        public float GetValueAtTime(int time)
        {
            foreach (var c in Curve)
            {
                if (time <= c.EndingPoint)
                    return c.Value;
            }

            if(_curve.Length > 0)
                throw new Exception("Value " + time + " out of bounds (" + _curve[0].StartingPoint + " , " + _curve[_curve.Length-1].EndingPoint + ")");
            throw new Exception("TimeCurve has no curve.");
        }

        [Pure]
        public int GetStartingTime()
        {
            Contract.Requires(0 <= Curve.Length);
            return Curve[0].StartingPoint;
        }

        [Pure]
        public int GetEndingTime()
        {
            Contract.Requires(0 <= Curve.Length - 1);
            return Curve[Curve.Length - 1].EndingPoint;
        }

        [Pure]
        public int GetTotalTime()
        {
            Contract.Requires(0 <= Curve.Length - 1);
            return Curve[Curve.Length - 1].EndingPoint - Curve[0].StartingPoint;
        }

        public static TimeCurve MakePriceCurve()
        {
            var ci = new List<CurveInterval>
            {

                new CurveInterval(0.039f,1,60),       // 1
                new CurveInterval(0.033f,61,120),     // 2
                new CurveInterval(0.025f,121,180),    // 3
                new CurveInterval(0.023f,181,240),    // 4
                new CurveInterval(0.022f,241,480),    // 5
                new CurveInterval(0.021f,481,540),    // 9
                new CurveInterval(0.022f,541,600),    // 10
                new CurveInterval(0.025f,601,720),    // 11
                new CurveInterval(0.027f,721,780),    // 13
                new CurveInterval(0.031f,781,840),    // 14
                new CurveInterval(0.03f,841,900),    // 15
                new CurveInterval(0.026f,901,960),    // 16
                new CurveInterval(0.023f,961,1140),   // 17
                new CurveInterval(0.025f,1141,1200),  // 20
                new CurveInterval(0.041f,1201,1260),  // 21
                new CurveInterval(0.036f,1261,1320),  // 22
                new CurveInterval(0.03f,1321,1380),  // 23
                new CurveInterval(0.035f,1381,1440)   // 24
            };

            return new TimeCurve(ci);
        }

        public static TimeCurve MakePowerCurve()
        {
            var ci = new List<CurveInterval>
            {

                new CurveInterval(3000,1,240),       // 1 (25)
                new CurveInterval(5000,241,720),    // 4 (28)
                new CurveInterval(4000,721,900),    // 7 (31)
                new CurveInterval(5000,901,1080),    // 10 (34)
                new CurveInterval(3000,1081,1200),    // 13 (37)
                new CurveInterval(6000,1201,1320),    // 15 (39)
                new CurveInterval(2000,1321,1440),   // 17 (41)
            };

            return new TimeCurve(ci);
        }
    }
}
