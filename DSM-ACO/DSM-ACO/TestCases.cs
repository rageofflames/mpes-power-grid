using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace DSM_ACO
{
	
	public class TestCases
	{
        public const int Testcase1 = 1;
	    public const int Testcase2 = 2;
	    public const int Testcase3 = 3;
	    public const int Testcase4 = 4;
	    public const int Testcase5 = 5;
	    public const int Testcase6 = 6;

	    public class Test
        {   
            public List<Task> Tasks;
            
            public double[] Times;
            public double TimesAverage;
            public double TimesStandardDeviation;
            
            public double[] Solutions;
            public double SolutionsStandardDeviation;
            public double SolutionsAverage;
            
            public Test(int numberOfRows, List<Task> tasks)
            {
                Tasks = tasks;
                
                Times = new double[numberOfRows];
                Solutions = new double[numberOfRows];
                
                TimesAverage = 0;
                TimesStandardDeviation = 0;
                
                SolutionsAverage = 0;
                SolutionsStandardDeviation = 0;
            }
            
            public void CalculateAverages()
            {
                TimesAverage = AverageFromArray(Times);
                SolutionsAverage = AverageFromArray(Solutions);
            }
            
            public void CalculateStandardDeviations()
            {
                TimesStandardDeviation = StandardDeviationFromArray(Times);
                SolutionsStandardDeviation = StandardDeviationFromArray(Solutions);
            }
        }
        
		public static void Main(string[] args)
        {
            
            int numberOfRuns = 10;
            
            //RunTestForNumberOfRuns(Testcase1, numberOfRuns);
            //RunTestForNumberOfRuns(Testcase2, numberOfRuns);
            //RunTestForNumberOfRuns(Testcase3, numberOfRuns);
            //RunTestForNumberOfRuns(Testcase4, numberOfRuns);
            RunTestForNumberOfRuns(Testcase5, numberOfRuns);
            //RunTestForNumberOfRuns(Testcase6, numberOfRuns);
        }
        
        private static void RunTestForNumberOfRuns(int testNumber, int numberOfRuns)
        {
            Console.WriteLine("(Test{0}) Starting", testNumber);
            
            Test test;
            
            switch (testNumber)
            {
                case Testcase1:
                    test = new Test(numberOfRuns, TestCase1());
                    break;
                case Testcase2:
                    test = new Test(numberOfRuns, TestCase2());
                    break;
                case Testcase3:
                    test = new Test(numberOfRuns, TestCase3());
                    break;  
                case Testcase4:
                    test = new Test(numberOfRuns, TestCase4());
                    break;  
                case Testcase5:
                    test = new Test(numberOfRuns, TestCase5());
                    break;    
                case Testcase6:
                    test = new Test(numberOfRuns, TestCase6());
                    break;
                default:
                    Console.WriteLine("No Test for number {0}", testNumber);
                    return;
            }
            
            Stopwatch stopwatch = new Stopwatch();
            
            for (int i = 0; i < numberOfRuns; i++)
            {                                                                                                        //32,0.7f,2,0.03f,1000,false,true,false
	                                                                                                                 //100,0.7f,2,0.1f,1000,false,true,false
                stopwatch.Start();                                                                                   //1000,1.62f,2,0.001f,1000,true,true,false
                                                                                                                     //32,1.4,2,0.0001,1000,true,true,true
                var sch = new SchedulerAco(test.Tasks.ToList(),TimeCurve.MakePriceCurve(), TimeCurve.MakePowerCurve(), 100,0.7f,2,0.1f,1000, false, true, false);         
                sch.Solve();
            
                stopwatch.Stop();
                
                test.Times[i] = stopwatch.Elapsed.Seconds;
                test.Solutions[i] = sch.bestSolution;
                
                stopwatch.Reset();
                
                Console.WriteLine("(Test{0}:Iteration {1}/{2}) Finished Successfully in {3} seconds with cost {4} euros", 
                                testNumber, i+1, numberOfRuns, test.Times[i], test.Solutions[i]);
            }
            
            test.CalculateAverages();
            test.CalculateStandardDeviations();
           
            Console.WriteLine("(Test{0}) Finished", testNumber);
            Console.WriteLine("(Test{0}) Statistical Values:", testNumber);
            Console.WriteLine("(Test{0}) Time Average: {1}", testNumber, test.TimesAverage);
            Console.WriteLine("(Test{0}) Time Standard Deviation: {1}", testNumber, test.TimesStandardDeviation);
            Console.WriteLine("(Test{0}) Solutions :: Min: {1} euros ; Max: {2} euros", testNumber, test.Solutions.Min(), test.Solutions.Max());
            Console.WriteLine("(Test{0}) Solutions Average: {1} euros", testNumber, test.SolutionsAverage);
            Console.WriteLine("(Test{0}) Solutions Standard Deviation: {1} euros", testNumber, test.SolutionsStandardDeviation);
        }
        
        private static double AverageFromArray(double[] array) 
        {
            return array.Average();
        }
        
        private static double StandardDeviationFromArray(double[] array) 
        {
            
            double average = AverageFromArray(array);
            double sumOfSquaresOfDifferences = array.Select(val => (val - average) * (val - average)).Sum();
            
            return Math.Sqrt(sumOfSquaresOfDifferences / array.Length); 
        }
        
        public static List<Task> TestCase1()
        {
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1, 210),
                Task.MakeMicrowaveTask("Microondas 2", 1276, 1440),
                Task.MakeOvenTask("Forno 1", 7, 165),
                Task.MakeOvenTask("Forno 2", 901, 1080),
                Task.MakeHeatPumpTask("Bomba 1", 481, 840),
                Task.MakeHeatPumpTask("Bomba 2", 481, 840),
                Task.MakeAirConditionerTask("Ar Condicionado 1", 781, 1070)
            };
        }
        
        public static List<Task> TestCase2()
        {
            Task[] dishwasher1Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 1", 226, 790);
            Task[] dishwasher2Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 2", 1141, 1440);
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1276, 1440),
                dishwasher1Tasks[0],
                dishwasher1Tasks[1],
                dishwasher1Tasks[2],
                dishwasher2Tasks[0],
                dishwasher2Tasks[1],
                dishwasher2Tasks[2],
                Task.MakeOvenTask("Forno 1", 7, 165),
                Task.MakeHeatPumpTask("Bomba 1", 481, 840),
                Task.MakeWashingMachineTask("Maquina de Lavar Roupa 1", 1, 960),
                Task.MakeAirConditionerTask("Ar Condicionado 1", 1320, 1430)
            };
        }
        
        public static List<Task> TestCase3()
        {
            Task[] dishwasher1Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 1", 226, 790);
            Task[] dishwasher2Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 2", 1141, 1440);
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1, 210),
                Task.MakeMicrowaveTask("Microondas 2", 1276, 1440),
                dishwasher1Tasks[0],
                dishwasher1Tasks[1],
                dishwasher1Tasks[2],
                dishwasher2Tasks[0],
                dishwasher2Tasks[1],
                dishwasher2Tasks[2],
                Task.MakeDryingMachineTask("Maquina de Secar 1", 1, 750),
                Task.MakeOvenTask("Forno 1", 7, 165),
                Task.MakeOvenTask("Forno 2", 901, 1080),
                Task.MakeHeatPumpTask("Bomba 1", 481, 840),
                Task.MakeHeatPumpTask("Bomba 2", 481, 840),
                Task.MakeWashingMachineTask("Maquina de Lavar Roupa 1", 1, 960),
                Task.MakeAirConditionerTask("Ar Condicionado 1", 1321, 1430),
                Task.MakeAirConditionerTask("Ar Condicionado 2", 1141, 1200),
                Task.MakeAirConditionerTask("Ar Condicionado 3", 1081, 1200),
                Task.MakeAirConditionerTask("Ar Condicionado 4", 811, 1070),
                Task.MakeAirConditionerTask("Ar Condicionado 5", 181, 300),
                Task.MakeAirConditionerTask("Ar Condicionado 6", 61, 300)
            };
        }
        
        public static List<Task> TestCase4()
        {
            Task[] esd1Task = Task.MakeEsdTask("ESD 1", 1, 1440);
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1, 60),
                Task.MakeMicrowaveTask("Microondas 2", 1381, 1440),
                Task.MakeDryingMachineTask("Maquina de Secar 1", 1, 240),
                Task.MakeWashingMachineTask("Maquina de Lavar Roupa 1", 1081, 1260),
                Task.MakeOvenTask("Forno 1", 841, 1020),
                esd1Task[0],
                esd1Task[1],
                esd1Task[2],
                esd1Task[3],
                esd1Task[4],
                esd1Task[5],
                esd1Task[6],
                esd1Task[7],
                esd1Task[8],
                esd1Task[9]
            };
        }
        
        public static List<Task> TestCase5()
        {
            Task[] esd1Task = Task.MakeEsdTask("ESD 1", 1, 1440);
            Task[] dishwasher1Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 1", 226, 790);
            Task[] dishwasher2Tasks = Task.MakeDishwasherTask("Maquina de Lavar Pratos 2", 1141, 1440);
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1, 210),
                Task.MakeHeatPumpTask("Bomba 1", 481, 840),
                Task.MakeHeatPumpTask("Bomba 2", 481, 840),
                dishwasher1Tasks[0],
                dishwasher1Tasks[1],
                dishwasher1Tasks[2],
                dishwasher2Tasks[0],
                dishwasher2Tasks[1],
                dishwasher2Tasks[2],
                esd1Task[0],
                esd1Task[1],
                esd1Task[2],
                esd1Task[3],
                esd1Task[4],
                esd1Task[5],
                esd1Task[6],
                esd1Task[7],
                esd1Task[8],
                esd1Task[9]
            };
        }

        public static List<Task> TestCase6()
        {
            return new List<Task>
            {
                Task.MakeMicrowaveTask("Microondas 1", 1, 60),
                Task.MakeMicrowaveTask("Microondas 2", 1381, 1440),
                Task.MakeMicrowaveTask("Microondas 3", 1, 210),
                Task.MakeMicrowaveTask("Microondas 4", 1276, 1440),
                Task.MakeOvenTask("Forno 1", 7, 165),
                Task.MakeOvenTask("Forno 2", 901, 1080),
                Task.MakeHeatPumpTask("Bomba 1", 481, 840),
                Task.MakeHeatPumpTask("Bomba 2", 481, 840),
                Task.MakeWashingMachineTask("Maquina de Lavar Roupa 1", 1, 960),
                Task.MakeWashingMachineTask("Maquina de Lavar Roupa 2", 1081, 1260),
                Task.MakeDryingMachineTask("Maquina de Secar 1", 1, 750),
                Task.MakeDryingMachineTask("Maquina de Secar 2", 1, 240),
                Task.MakeAirConditionerTask("Ar Condicionado 1", 1321, 1430),
                Task.MakeAirConditionerTask("Ar Condicionado 2", 1141, 1200),
                Task.MakeAirConditionerTask("Ar Condicionado 3", 1081, 1200),
                Task.MakeAirConditionerTask("Ar Condicionado 4", 811, 1070),
                Task.MakeAirConditionerTask("Ar Condicionado 5", 181, 300),
                Task.MakeAirConditionerTask("Ar Condicionado 6", 61, 300)
            };
        }
	}
    
}