﻿using System.Collections.Generic;
using System.Web.Http;
using DSM_ACO;
using MPES_API.Models;

namespace MPES_API.Controllers
{
    public class SchedulerController : ApiController
    {
        public SchedulerResults GetSchedule(int testCase)
        {
            List<Task> test = new List<Task>();
            switch (testCase)
            {
                case TestCases.Testcase1:
                    test = TestCases.TestCase1();
                    break;
                case TestCases.Testcase2:
                    test = TestCases.TestCase2();
                    break;
                case TestCases.Testcase3:
                    test =TestCases.TestCase3();
                    break;
                case TestCases.Testcase4:
                    test = TestCases.TestCase4();
                    break;
                case TestCases.Testcase5:
                    test =TestCases.TestCase5();
                    break;
                case TestCases.Testcase6:
                    test = TestCases.TestCase6();
                    break;
            }

            var sch = new SchedulerAco(test, TimeCurve.MakePriceCurve(), TimeCurve.MakePowerCurve(), 32, 1.4f, 2, 0.0001f, 1000, true, true, true);

            var solution = sch.Solve();

            var results = new SchedulerResults(solution, sch.Pricing, sch.Power);
            return results;
        }
    }
}
