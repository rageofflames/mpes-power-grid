﻿using System.Collections.Generic;
using DSM_ACO;

namespace MPES_API.Models
{
    public class GraphData
    {
        public int[] SolutionCurveX;
        public int[] PowerX;
        public int[] PricingX;

        public int[] SolutionValues;
        public double[] PowerValues;
        public double[] PricingValues;

        public GraphData(SchedulerAco.Ant solution, TimeCurve pricing, TimeCurve power)
        {
            var tasksX = new List<int>();
            var tasksConsumption = new List<int>();

            var values = new int[1440];

            var previousV = -10;

            for (var i = 0; i < values.Length; i++)
            {
                foreach (var st in solution.StartingTimes)
                {
                    if (st.Value.Start <= i && st.Value.End > i)
                    {
                        values[i] += (int)st.Key.ConsumptionCurve.GetValueAtTime(1 + i - st.Value.Start);
                    }
                }

                if (previousV != values[i])
                {
                    if (previousV != -10)
                    {
                        tasksX.Add(i - 1);
                        tasksConsumption.Add(previousV);
                    }

                    tasksX.Add(i);
                    tasksConsumption.Add(values[i]);
                    previousV = values[i];
                }
            }

            List<int> powX = new List<int>();
            List<double> pow = new List<double>();
            foreach (var period in power.Curve)
            {
                powX.Add(period.StartingPoint - 1);
                pow.Add(period.Value);
                powX.Add(period.EndingPoint - 1);
                pow.Add(period.Value);
            }

            List<int> prcX = new List<int>();
            List<double> prc = new List<double>();
            foreach (var period in pricing.Curve)
            {
                prcX.Add(period.StartingPoint - 1);
                prc.Add(period.Value);
                prcX.Add(period.EndingPoint - 1);
                prc.Add(period.Value);
            }

            SolutionCurveX = tasksX.ToArray();
            SolutionValues = tasksConsumption.ToArray();
            PricingX = prcX.ToArray();
            PricingValues = prc.ToArray();
            PowerX = powX.ToArray();
            PowerValues = pow.ToArray();
        }
    }
}