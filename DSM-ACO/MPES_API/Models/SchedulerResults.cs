﻿using System;
using DSM_ACO;

namespace MPES_API.Models
{
    public class SchedulerResults
    {
        public SchedulerAco.Ant Solution;
        public String[] StartingHours;
        public double Cost;
        public GraphData GraphLines;

        public SchedulerResults(SchedulerAco.Ant solution, TimeCurve pricing, TimeCurve power)
        {
            Solution = solution;
            Cost = solution.Cost()/60000;
            StartingHours = solution.TaskHours().ToArray();
            GraphLines = new GraphData(solution,pricing,power);
        }
    }
}