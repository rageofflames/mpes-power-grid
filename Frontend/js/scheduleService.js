app.factory('ScheduleApi', ['$http'/*, '$cookies', '$location'*/, function($http /*, $cookies, $location*/) {
  var server = {
    url: 'http://localhost:64730'
  };

  return {
    getSchedule: function (testCase) {
      return $http({ cache: false,
        url: server.url + '/api/Scheduler?testCase=' + testCase
      });
    }
  };   
  
}]);
