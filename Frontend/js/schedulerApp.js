var app = angular.module("schedulerApp", [
		'ui.router'
	]);

app.config(['$stateProvider', '$urlRouterProvider',
function ($stateProvider, $urlRouterProvider) {
  'use strict';

  // Set the following to true to enable the HTML5 Mode
  // You may have to set <base> tag in index and a routing configuration in your server
  //$locationProvider.html5Mode(false);

  // default route
  $urlRouterProvider.otherwise('/app');

  // 
  // Application Routes
  // -----------------------------------   
  $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: '../index.html',
        controller: 'mainController',
        //resolve: helper.resolveFor('modernizr', 'icons', 'toaster', 'c3', 'lodash', 'whirl', 'ngTable')
    });


}]);
