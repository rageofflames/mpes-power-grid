app.controller("mainController", function($scope, ScheduleApi) {
	$scope.text = "Choose a Test: ";
	$scope.consumptionX = [];
    $scope.pricingX = [];
	$scope.powerX = [];
	$scope.consumption = [];
	$scope.pricing = [];
	$scope.power = [];
	$scope.solution = null;
	$scope.solutionCost = null;
	$scope.taskHours = null;
	
	$scope.selectedTest = 1;
	$scope.testOptions = [{
							number: 1,
							descr: "Teste 1"
							},
							{
							number: 2,
							descr: "Teste 2"
							},
							{
							number: 3,
							descr: "Teste 3"
							},
							{
							number: 4,
							descr: "Teste 4"
							},
							{
							number: 5,
							descr: "Teste 5"
							},
							{
							number: 6,
							descr: "Teste 6"
						}];
	
	$scope.calculate = function() {
		$scope.text = "Calculating schedule for test: " + $scope.testOptions[$scope.selectedTest-1].descr;
		ScheduleApi.getSchedule($scope.selectedTest)
		.success(function(data) {
			$scope.consumptionX = [];
			$scope.pricingX = [];
			$scope.powerX = [];
			$scope.consumption = [];
			$scope.pricing = [];
			$scope.power = [];
			$scope.solution = data.Solution;
			$scope.solutionCost = data.Cost;
			$scope.taskHours = data.StartingHours;
			
			console.log(data);
			var pricingValues = data.GraphLines.PricingValues;
			for(var i=0; i<pricingValues.length; i++) {
			    pricingValues[i] *= 300000;
			}
			
			$scope.consumptionX[0] = 'ConsumptionX';
			$scope.consumptionX = $scope.consumptionX.concat(data.GraphLines.SolutionCurveX);
			
			$scope.powerX[0] = 'PowerX';
			$scope.powerX = $scope.powerX.concat(data.GraphLines.PowerX);
			
			$scope.pricingX[0] = 'PricingX';
			$scope.pricingX = $scope.pricingX.concat(data.GraphLines.PricingX);
			
			$scope.consumption[0] = 'Consumption';
			$scope.consumption = $scope.consumption.concat(data.GraphLines.SolutionValues);
			
			$scope.pricing[0] = 'Pricing';
			$scope.pricing = $scope.pricing.concat(pricingValues);
			
			$scope.power[0] = 'Power';
			$scope.power = $scope.power.concat(data.GraphLines.PowerValues);
			
			var chart = c3.generate({
			    bindto: '#chart',
			    data: {
					xs: {
						'Power':'PowerX',
						'Pricing':'PricingX',
						'Consumption':'ConsumptionX'
					},
				    columns: [
				        $scope.consumptionX,
				        $scope.pricingX,
						$scope.powerX,
						$scope.consumption,
						$scope.pricing,
						$scope.power
				    ]
			    },
				point: {
				  r: 0
				},
				axis: {
				  x: {
				    tick: {
				      fit: true,
					  count: 24,
					  values: [0,60,120,180,240,300,360,420,480,540,600,660,720,780,840,900,960,1020,1080,1140,1200,1260,1320,1380,1440]
				    }
				  }
				}
			});
		
			$scope.text = "Solution Calculated!";
		});
	}
});