A scheluding program in C#, using an Ant Colonization algorithm to create schedules for appliances in a Smart Grid. The application has a web interface written in ASP.NET to facilitate running the different test configurations and analysising the results.

Developed by João Marinheiro and André Silva for the Planning and Scheduling Methods course at FEUP.